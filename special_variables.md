`$_` - the last argument that you used

```
cat myfile.txt
...
...
file $_
```

It is not listed here though:\
https://www.gnu.org/software/bash/manual/html_node/Special-Parameters.html
