`grep -o` might be helpful:
```
curl -s https://developer.mozilla.org/en-US/docs/Learn/HTML/Tables/Basics | grep -o '<td>.*</td>'
```

`-o, --only-matching`:\
Print only the matched (non-empty) parts of a matching line, with each such part on a separate output line.

In combination with the regex `.*` `grep -o` seems to be a very useful tool.

#### remove parts of the string with sed

```
$ echo "\"this is my string\""
"this is my string"
```

```
$ echo "\"this is my string\"" | sed 's/\"//g'
this is my string
```

```
$ echo "this is my string is that true?" | sed 's/is//g'
th  my string  that true?
```