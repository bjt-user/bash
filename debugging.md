#### bash -x

You can give option to the `bash` command when executing a script.\
Or you can give those options in the "shebang" line.

You can debug a bash script with
```
bash -x myscript.sh
```
This will execute the entire script and show the code with output.\
(or write `#!/bin/bash -x` in the first line of the script and execute it with `./script.sh`)

You can't execute the script step by step.

You can't set breakpoints. (only with a uncomfortable workaround)

If you want to see line numbers add this:
```
PS4='$LINENO: '
```

If you want to debug only parts of the script you can write
```
set -x
```
At the start of that section and
```
set +x
```
at the end of the section.\
(it would be nice if you could also silence the normal output of the rest of the script)

#### set -e

`set -e` is very useful in combination with `set -x`.

https://gist.github.com/vncsna/64825d5609c146e80de8b1fd623011ca

> The set -e option instructs bash to immediately exit if any command  has a non-zero exit status.\
You wouldn't want to set this for your command-line shell, but in a script it's massively helpful.\
In all widely used general-purpose programming languages,\
an unhandled runtime error - whether that's a thrown exception in Java,\
or a segmentation fault in C, or a syntax error in Python - immediately halts execution of the program;\
subsequent lines are not executed.

> By default, bash does not do this.\
This default behavior is exactly what you want if you are using bash on the command line \
you don't want a typo to log you out! But in a script, you really want the opposite.\
If one line in a script fails, but the last line succeeds,\
the whole script has a successful exit code. That makes it very easy to miss the error.\
Again, what you want when using bash as your command-line shell and using it in scripts are at odds here.\
Being intolerant of errors is a lot better in scripts, and that's what set -e gives you.



#### check for syntax errors:
Sometimes it is convenient to just check for syntax errors instead of executing the entire script and have a large output and actions that you might not want to happen:
```
bash -n myscript.sh
```

#### exit scriped when pipes fail

This can be helpful when piped commands exit with rc unequal zero:
```
set -o pipefail
```
The script will then exit if a pipe fails.

#### check for unset variables

```
bash -u myscript.sh
```
I tried a string concatenation with a variable that doesn't exist and that does not give an error.\
But it seems to work good in other situations where you use a variable that was not declared before.

You can put this at the beginning of a bash script so that the script exits when it hits a undefined variable:
```
set -o nounset
```

But this will exit the script even if you just check if an unset variable exists...

#### printf debugging

```
echo "${LINENO}: test"
```

#### autoformatting

This is also useful when dealing with messy coworkers.

```
sudo pacman -Ss shfmt
```

```
shfmt -w bash_file.sh
```

#### stopping at every line

You can put `trap read DEBUG` anywhere.\
Then execute the script and it will stop at every line from that point on.\
To continue hit <kbd>enter</kbd>.

```
#!/bin/bash

echo 'hi'

trap read DEBUG

echo 'what is this?'

exit 0
```

to untrap:
```
trap - DEBUG
```

This only really makes sense if you execute the script with `-x` \
because variable declarations will otherwise require you to confirm \
a blank line.

This might be used like a breakpoint for poor people...

## dealing with messy code from coworkers

#### tackling cds

When you have to deal with aweful code that uses `cd` in scripts.\
And you don't have the time to rewrite the script entirely.\
You could use something like this to make the script more verbose:

```
mv -v $(realpath ../ansible_tutorial.mp4) $(realpath .)
```

You should be able to do this for `rm`, `mv`, and `cp` commands.

#### exiting on rc unequal 0

Exiting instantly when a command fails:
```
mv -v foo1 testf/. || exit 1
```
