```
ssh myserver 'cat /etc/os-release'
```

***
#### remote command and run as another user

```
ssh myserver 'su -c "touch /home/myuser/generated_through_script.txt" myuser'
```

***

#### run multiple remote commands

```
#!/bin/bash

ssh myhost /bin/bash << EOF
  cat /etc/os-release
  touch "/tmp/myspecialfile.txt"
  touch "/home/myuser/myspecialfile.txt"
EOF

exit 0
```

https://stackoverflow.com/questions/4412238/what-is-the-cleanest-way-to-ssh-and-run-multiple-commands-in-bash

***

#### you must have a tty to run sudo

https://unix.stackexchange.com/questions/122616/why-do-i-need-a-tty-to-run-sudo-if-i-can-sudo-without-a-password

That's probably because your /etc/sudoers file (or any file it includes) has:
```
Defaults requiretty
```
Seems to only be a default setting at old Red Hat systems.

***
