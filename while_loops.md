Example of a while loop:
```
i=0

while [ $i -le 2 ]
do
  echo Number: $i
  ((i++))
done
```
