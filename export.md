With the `export` command you can define `environment variables`.

You can also export functions to the environment:
```
export -f <functionname>
```
*Hint: Write the function name without `()`* 

For example you could define a function on the command line and then call it in any script:
```
#!/bin/bash

#define a function in the command line
#by typing:
#$ function printok() {
#$ printf "\nok\n"
#$ }

#now you can call this function on the cli by typing "printok"

#then type
#export -f printok

#now you should be able to call this function in any script
#and see it after typing "env"

printok
```

see all environemnt variables:
```
env
```

remove an environment variable or function:
```
unset <functionname/variablename>
```
