#### no exit in sourced files

You cannot have an `exit` in your sourced file.\
That will exit your program.\
Probably a sourced file will not open a subshell.


#### using relative paths in your source commands

You may want to use relative paths in your source commands,\
because then you can navigate easily with `gf` to your sourced script in vim.

But you need to get the path of the main script with a complicated command \
and then `cd` into it like this:
```
#!/bin/bash

script_path=$(cd "$(dirname "${BASH_SOURCE[0]}")"; pwd -P)

cd "$script_path"

source ./hw.sh

hw

exit 0
```
In this example `hw.sh` is in the same dir as the main script.

If you dont do it that way you cannot call the script from anywhere.\
Without this `cd` you could only call it from the dir the scripts are in.
