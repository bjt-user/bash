#### $0 vs ${BASH_SOURCE[0]}

${BASH_SOURCE[0]} (or, more simply, $BASH_SOURCE[1] ) contains the (potentially relative) path of the containing script in all invocation scenarios, notably also when the script is sourced, which is not true for $0.

https://stackoverflow.com/questions/35006457/choosing-between-0-and-bash-source

`$0` is part of the POSIX shell specification, whereas `BASH_SOURCE`, as the name suggests, is Bash-specific.

#### relative path + file name of current script
```
scriptpath=$0
```
This will contain the relative path + the file name of the currently executed script.


#### get full path of the script you are running

`realpath` is program that seems to be installed on most Linux machines.\
It is part of GNU but apparently not part of the POSIX standard.

When `realpath` is installed you can do this:
```
myreal=$(realpath "$0")
```
This will get the full path + the file name of the currently executed script.\
If the file is a symbolic link it will print the path and file name of the original file.

You can use `-s` option to not print the symlinked file and just print the path to the link:
```
realpath -s scripts/linkedscript.sh
```
output:
```
/home/bf/scripts/linkedscript.sh
```

if you only want to get the directory:
```
SCRIPT_PATH=$(dirname $(realpath -s $0))
```
