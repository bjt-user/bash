#### show current time

```
date -d "now" +%H%M%S
```

with nanoseconds (you can use this command as a stopwatch in scripts to measure performance)
```
date -d "now" +%H:%M:%S:%N
```
or
```
date +"%T.%3N"
```
returns the current time with nanoseconds rounded to the first 3 digits, which is milliseconds.

show current time and date in unix time stamp:
```
date -d "now" +%s
```

#### show and format current date

```
date -d "now" +%Y%m%d
```

***
#### convert timestamp to human readable date

```
date -d @1668153629
```

or like this:
```
date -d @1668153629 +%H%M%S
```
***

