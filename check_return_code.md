You can use
```
echo $?
``` 

Shellcheck does not like that though: https://www.shellcheck.net/wiki/SC2181

example:
```
#!/bin/bash

if curl -sI https://www.google.com; then
  echo "succeeded."
else
  echo "failed."
fi

if curl -sI mywebsite; then
  echo "succeeded."
else
  echo "failed."
fi

exit 0
```
=> for some reason this breaks syntax highlighting in vim...
