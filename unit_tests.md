## bats

```
sudo pacman -S bats
```

TODO: Check out these additional packages:
```
extra/bats-assert 2.1.0-2
    Common assertions for Bats
extra/bats-file 0.4.0-1
    Common filesystem assertions for Bats
extra/bats-support 0.3.0-5
    Supporting library for Bats test helpers
```

## assert.sh

Doesnt seem to support mocks.

```
git clone https://github.com/lehmannro/assert.sh.git
```

There is one file called `assert.sh` and that is the only file you need to source.

#### example

You have to escape `$` signs and of course double quotes \
in the quoted commands.
```
#!/bin/bash

source assert.sh

assert "echo \"test\"" "test"

assert "echo \"test\"" "foo"

assert "date | awk '{ print \$7 }'" "2024"

assert "cat /etc/os-release | wc -l" "12"

assert_end my_section

exit 0
```

You can use `assert_end` multiple times for multiple test sections.

#### assert_raises (check for error codes that your function returns)

If your function returns 1 when the input string is empty:
```
assert_raises "remove_extension \"\"" 1
```

If your function does not explicitly return an error code, bash seems to assume rc 0.
