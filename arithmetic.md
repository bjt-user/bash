#### increment a variable

WARNING: When incrementing a zero value you need to use \
`((++mynum))` (put plus signs infront of variable) \
because `((mynum++))` will result in rc 1!

```
#!/bin/bash

mynumber=2

((mynumber++))

printf "%s\n" "$mynumber"
```
Note that you do not use a `$` symbol in the incrementation which is surprising.


#### assign the value of another variable plus one to a variable

```
#!/bin/bash

myconst=2

nextvalue=$((myconst+1))

printf "\$myconst is %s, \$nextvalue is %s\n" "$myconst" "$nextvalue"
```
You need a dollar sign before the two `(`s in this case which is weird.
