One-line comments start with a `#`.

Multi-line comments work like this:
```
#!/bin/bash

printf "line 1\n"
: '
printf "line 2 (which should not be printed)\n"
printf "line 3 (which should not be printed)\n"
printf 'single quote test\n'
'
printf "line 4\n"
```
