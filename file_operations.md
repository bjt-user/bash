#### check if a file exists

```
FILE="fileexists.sh"
if [ -f "$FILE" ]; then
  echo "$FILE exists."
fi
```

```
FILE="fileexists.sh"
if [ ! -f "$FILE" ]; then
  echo "$FILE does not exist."
fi
```

#### check if a file with a certain ending exists?

```
#redirect error messages when there are 0 files
number_of_sh_files=$(ls *.sh 2>/dev/null | wc -l)

printf "number of sh files: %s\n" "$number_of_sh_files"

if [[ ${number_of_sh_files} -eq 0 ]]; then
  printf "no .sh files"
else
  printf "Shell files exist.\n"
fi
```

#### remove file extensions

```
textfiles=$(ls *.txt 2>/dev/null)

textfiles=$(basename -s .txt ${textfiles})

printf ".txt-files without extensions: %s\n" "$textfiles"
```

#### read file character by character

```
count=1
while read -n1 char
do
  echo -n "iteration number $count:"
  echo " $char"
  (( count++ ))
done < test.txt
```

#### read file line by line

This reads a file named `input.txt` and outputs only the lines that have a `hello` in them.
```
#!/bin/bash

regex="hello"

while read line
do
  if [[ $line =~ $regex ]]; then
    echo $line
  fi
done < input.txt
```

#### write to file

overwrite everything in that file
```
printf "some words\n" > ~/Documents/test.txt
```

append to the end of the file
```
printf "some words\n" >> ~/Documents/test.txt
```

**PITFALL: NEVER USE `~` IN PATH VARIABLE NAMES! IT WILL NOT WORK! USE `${HOME}` INSTEAD!**\
for example
```
...
CONFIGFOLDER="${HOME}/.icons/default"
printf "Inherits=%s" "$theme" >> "${CONFIGFILEPATH}"
```


#### get name of the current script

```
scriptname=$(basename "$0")
```

#### get the extension of a file
```
$ foo=podman-4.5.0-setup.exe
$ echo ${foo##*.}
exe
```

#### get the file name without its extension
```
echo ${foo%.*}
```
