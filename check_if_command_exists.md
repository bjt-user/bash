```
if type firefox &>> /dev/null; then
  echo "you have firefox installed."
fi
``` 

#### example function

```
function check_command() {
        local binary
        binary="$1"

        if ! type "$binary" &>>/dev/null; then
                echo "The executable ${binary} is not installed."
                exit 1
        fi
}
```

You can call it like this:
```
check_command "yq"
```
