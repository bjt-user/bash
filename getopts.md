```
man bash
```
search for `getopts` with `/getopts`

Here is another page: https://man7.org/linux/man-pages/man1/getopts.1p.html

It looks like you can use it to parse cli arguments and set flags accordingly.

#### example
```
#!/bin/bash

while getopts u:a:f: flag
do
    case "${flag}" in
        u) username=${OPTARG};;
        a) age=${OPTARG};;
        f) fullname=${OPTARG};;
    esac
done

echo "Username: $username";
echo "Age: $age";
echo "Full Name: $fullname";
```

You have to call this script like this:
```
./scriptname -u Michael -f Smith -a 55
```
The order of arguments doesn't matter but you can't use `--username` instead of `-u`.\
=> that is a real downside of this builtin function
