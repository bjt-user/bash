#### calling a function

To call a function (whether it is a bash function or a function that you have written) and save the return value in a variable:
```
DUR1=$(datesubtracter "${DATE1}" "${DATE2}")
```
The variables `$DATE1` and `$DATE2` are variables that are passed as arguments to that function.\
You might not necessarily need those curly brackets.

#### declaring a function
```
#!/bin/bash

function printf_test () {
  printf "test\n"
}

printf_test
```

Having a function that consists only of comments will give a syntax error:\
(which is unexpected)
```
#!/bin/bash

function printf_test () {
  #printf "test\n"
}

printf_test
```
syntax-error:
```
./function_comment.sh: line 5: syntax error near unexpected token `}'
./function_comment.sh: line 5: `}'
```

## return value

A function can have *one* return value.\
You can return a value with `echo $somevariable`.

```
#!/bin/bash

function add_two() {
  ((result=$1+2))
  echo $result
}

myvar=$(add_two 7)

printf "%s\n" "$myvar"
```

or use `printf`

I found out that when you don't use `echo` and have some `printf`s in the function, the variable you assign the function to will contain all the output that is printed to the terminal.

#### return keyword

You can return from a function before its end with the `return` keyword.\
And you can set a return value of that function, but that return value will not be saved in variables.\
And the you can only use `return` with numeric values.

```
#!/bin/bash

function return_var() {
	local my_var
	my_var=1

	echo "test"

	return 1

	echo "second test"
}

buffer=$(return_var)

echo ${buffer}

return_var
echo "return value of the function is ${?}"

exit 0
```
`buffer` will take on the string "test" and not the return value from the `return` keyword.

#### function arguments

A function can take *multiple* arguments.\
You catch those arguments with `$1` for the first, `$2` for the second and so on.\
(like you do with command line arguments)

This example shows the *argument* `7` is given to *function* `add_two`:
```
#!/bin/bash

function add_two() {
  ((result=$1+2))
  echo $result
}

myvar=$(add_two 7)

printf "%s\n" "$myvar"
```

#### printing to the screen without having it as a return value

Inside a function everything that is printed to the screen is also the return value of that function.\
Sometimes you might want to tell the user something or ask for user input and not have that as a return value.\
The only solution I found so far is to redirect to `/dev/tty`:
```
function test {
  echo "what is your name?" > /dev/tty
  read name
  echo $name
}
```

#### call by reference

At least in Bash, as of Bash 4.3-alpha, you can use namerefs to pass function arguments by reference:
```
function boo()
{
    local -n ref=$1
    ref='new'
}

SOME_VAR='old'
echo $SOME_VAR # -> old
boo SOME_VAR
echo $SOME_VAR # -> new
```
