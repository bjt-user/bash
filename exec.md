From manpages
```
exec [-cl] [-a name] [command [arguments]]
If command is specified, it replaces the shell.  No new process is created.  The arguments become the arguments to command.
```

https://askubuntu.com/questions/525767/what-does-an-exec-command-do

If you run exec by itself, without a command, it will simply make the redirections apply to the current shell. You probably know that when you run command > file, the output of command is written to file instead of to your terminal (this is called a redirection). If you run exec > file instead, then the redirection applies to the entire shell: Any output produced by the shell is written to file instead of to your terminal. For example here

```
bash-3.2$ bash
bash-3.2$ exec > file
bash-3.2$ date
bash-3.2$ exit
bash-3.2$ cat file
Thu 18 Sep 2014 23:56:25 CEST
```

#### example script

```
#!/bin/bash

# the output of the entire script will be put into exec.log and not onto the screen
exec > exec.log

printf "hello\n"
printf "will this be printed to the terminal?\n"
printf "or into the logfile?\n"

hostname
```
The output goes into the file `exec.log`.\
Nothing will be printed to the screen.\
After script execution everything will be printed to screen again.

You could do `exec >> exec.log` to append it to the end of the log file.

#### pitfall

When redirecting the output of the script to a file with `exec >> $logfile` you will not be able to see debugging terminal output when the script is started with `bash -x`.
