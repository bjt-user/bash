#!/bin/bash

# clone a git repo without history and .git folder
# so you will just get the pure files

repo=$1
foldername=$(cut -d'/' -f5 <<<$repo)
foldername=$(cut -d'.' -f1 <<<$foldername)
gitfolder=$foldername"/.git"

git clone --depth=1 $repo
rm -rf $gitfolder
