#!/bin/bash

declare -a myhosts
myhosts=($(cat ~/.ssh/config | grep "Host " | awk {'print $2'} | xargs))

for host in "${myhosts[@]}"; do
  echo $host
  ssh ${host} 'head -2 /etc/os-release'
done

exit 0
