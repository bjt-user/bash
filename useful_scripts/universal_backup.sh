#!/bin/bash

# UNDER CONSTRUCTION

declare -a hosts

while read line
do
  if [[ ${line:0:5} == "Host " ]]; then
    echo $line
    hosts+=(${line:5})
  fi
done < ~/.ssh/config

echo
echo "these are all hosts in ~/.ssh/config:"
i=0
for element in "${hosts[@]}"
do
  ((i++))
  printf "host %s: " "$i"
  printf "%s\n" "$element"
done

echo
read -p "now choose the destination for syncing your files! "

destination=${hosts[$REPLY-1]}

printf "you chose host: %s.\n" "$destination"

exit 0
