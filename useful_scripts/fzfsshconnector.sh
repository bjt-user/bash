#!/bin/bash

remotehost=$(cat ~/.ssh/config | grep "Host " | fzf)
remotehost=${remotehost:5}

printf "The selected host is %s.\n" "$remotehost"

ssh "$remotehost"

exit 0
