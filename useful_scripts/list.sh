#!/bin/bash


LIST_PATH=~/Documents/shoppinglist.txt


function add() {
  local section
  local content

  # multi line user input until user enters ""

  local inputs
  declare -a inputs

  while read line
  do
    if [[ $line == "" ]]; then
      break
    fi
    inputs+=("$line")
  done < "${1:-/dev/stdin}"

  section=$(count_sections)
  # add one to section to write the next section
  ((section++))
  printf "section %s\n" "$section" >> $LIST_PATH
  for input in "${inputs[@]}"
  do
    printf "%s\n" "$input" >> $LIST_PATH
  done
  printf -- "--------------------------------------------------------------------\n" >> $LIST_PATH
}

function delete() {
  printf "Which section do you want to delete?\n"
  read sectionchoice
  local section_string="section "$sectionchoice
  printf "Ok, so you want to delete %s.\n" "$section_string"
  
  local d_confirmation
  read_section $sectionchoice
  printf "Are you sure you want to delete this section? (Y/y)\n"
  read d_confirmation
  if [[ $d_confirmation == "y" || $d_confirmation == "Y" ]]; then
    printf "Continuing deletion.\n"
  else
    printf "Aborting deletion.\n"
    exit 1
  fi

  local found_flag=0
  local delete_from=0
  local delete_to=0

  line_number=0
  while read line
  do
    ((line_number++))
    if [[ $line =~ $section_string ]]; then
      printf "%s is in line %s.\n" "$section_string" "$line_number" 
      # we found the section
      found_flag=1
      delete_from=$line_number
    fi
    if [[ $found_flag -eq 1 ]] && [[ $line =~ "-----" ]]; then
      delete_to=$line_number
      printf "Delete until line %s.\n" "$line_number"
      break
    fi
  done < $LIST_PATH

  if [ $found_flag ]; then
    printf "Now we will delete.\n"
    printf "found_flag is %s\n" "$found_flag"
  else
    printf "That section does not exist.\n"
    printf "found_flag is %s\n" "$found_flag"
    exit 1
  fi

  sed --in-place "${delete_from},${delete_to}d" ${LIST_PATH}

  rename_sections
}

function count_sections() {
  total_sections=0

  while read line
  do
    if [[ $line =~ "-----" ]]; then
      ((total_sections++))
    fi
  done < $LIST_PATH

  echo $total_sections
}

function read_section() {
  local section=$1

  # check if $section is empty
  if [[ -z $section ]]; then
    printf "No section to read was provided.\n"
    exit 1
  else
    printf "Proceeding to read section %s.\n" "$section"
  fi

  local section_string="section "$section
  local found_flag=0

  local line_number=0
  while read line
  do
    ((line_number++))
    if [[ $line =~ $section_string ]]; then
      printf "%s is in line %s.\n\n" "$section_string" "$line_number" 
      # we found the section
      found_flag=1
    fi
    if [[ $found_flag -eq 1 ]]; then
      printf "%s\n" "$line"
    fi
    if [[ $found_flag -eq 1 ]] && [[ $line =~ "-----" ]]; then
      break
    fi
  done < $LIST_PATH
}

function rename_sections() {
  local section_counter=0
  local section_number
  local line_number=0
  while read line
  do
    ((line_number++))
    if [[ $line =~ "section" ]]; then
      # we found a section
      ((section_counter++))
      section_number=${line:8:9}
      printf "section %s is in line %s and is called %s\n" "$section_counter" "$line_number" "$section_number"
      local section_counter_string="section "$section_counter
      if [[ "$line" == "$section_counter_string" ]]; then
        printf "section %s in line %s is labeled correctly.\n" "$section_number" "$line_number"
      else
        printf "section %s in line %s is labeled WRONG.\n" "$section_number" "$line_number"
      fi
    fi
    sed -i "${line_number}s/${section_number}/${section_counter}/" ${LIST_PATH}
  done < $LIST_PATH
}

case $1 in
  add)
    add
    ;;
  count)
    count_sections
    ;;
  delete)
    delete
    ;;
  read)
    read_section $2
    ;;
  rename)
    rename_sections
    ;;
  vim)
    vim "$LIST_PATH"
    ;;
  *)
    printf "You want to see the shopping list.\n"
    less ~/Documents/shoppinglist.txt
    ;;
esac
