#!/bin/bash

# description: Print a .mbox file with html content (Evolution seems to have problems printing those emails)
# (this solution worked for 1 .mbox file with html content, might not work for all)

FILENAME=$(basename "$1")
FILENAMENOEXT="${FILENAME%.*}"
NEWFILE="${FILENAMENOEXT}.html"

# delete old output file if it already exists
if [ -f $NEWFILE ]; then
  rm $NEWFILE
fi


# cut out everything but html content and save it in the new file

html_start_string="<!DOCTYPE HTML"

html_flag=0

while read line
do
  if [[ $line =~ $html_start_string ]]; then
    html_flag=1
  elif [[ $line =~ "--=" ]]; then
    html_flag=0
  fi
  if [ $html_flag -eq 1 ]; then
    printf "%s" "${line}" >> "${NEWFILE}"
  fi
done < $FILENAME

# render the html in firefox and you can print from there
firefox $NEWFILE

# todo: instead of going into firefox and printing from there you can probably just do
# `lp $NEWFILE`
