#!/bin/bash

YT_URL="$1"

format_line=$(yt-dlp -F "${YT_URL}" |
	grep -v '^\[.*\]' | fzf --no-sort --reverse)

echo "format_line: ${format_line}"

format_identifier=${format_line%% *}

echo "format_identifier: ${format_identifier}"

read -r -p 'Choose a file name: ' file_name

yt-dlp -f "$format_identifier" "$YT_URL" -o "$file_name"

exit 0
