#!/bin/bash

filename="$1"

function tidy_string() {
	name="$1"
	name="${name// /_}"
	name="${name//\'/}"

	# remove brackets and things that are enclosed by brackets
	name="${name//\[*\]/_}"
	name="${name//\[/_}"
	name="${name//\]/_}"

	echo "$name"
}

new_name=$(tidy_string "$filename")

mv -v "$filename" "$new_name"

exit 0
