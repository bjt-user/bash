#!/bin/bash

echo "What is your question to the openai?"
read question
#question="How does a transistor work?"
#question="How can I make printf treat "\n" as a new line in a variable?"

temperature=0
max_tokens=1000

answer=$(curl -s https://api.openai.com/v1/completions -H "Content-Type: application/json" \
-H "Authorization: Bearer $(pass openai-apikey)" \
-d '{"model": "text-davinci-003", "prompt": "'"${question}"'", "temperature": '$temperature', "max_tokens": '$max_tokens'}')

answer=$(echo $answer | jq '.choices' | jq '.[0]' | jq '.text')
answer=${answer:1:-1}

echo -e $answer

exit 0
