#!/bin/bash

echo "Which command do you want to run on multiple hosts?"
# -r so that a backslash is treated as an actual backslash and not as escape character
read -r my_command
echo

remotehosts=($(cat ~/.ssh/config | grep "Host " | cut -d" " -f2 | fzf -m --reverse | xargs))

i=0
for remotehost in "${remotehosts[@]}"
do
  ((i++))
  printf "%s:\n" "$remotehost"
  ssh "$remotehost" "${my_command}"
  echo
done

exit 0
