#!/bin/bash

# usage: watch bash tagesschau2.sh

document=$(curl -s https://www.tagesschau.de/wirtschaft/boersenkurse/)

printf "DAX: "
printf "%s" "$document" | grep -i "pkt" | xargs | cut -d">" -f2 | cut -d"<" -f1

echo

printf "DOW JONES: "
printf "%s" "$document" | grep -i -A6 -B2 "<span class=\"name\">DOW JONES</span>" | cut -d">" -f3 | cut -d"<" -f1 | xargs

echo

printf "BRENT: "
printf "%s" "$document" | grep -i -A7 -B2 "<span class=\"name\">BRENT</span>" | grep -i "price" | cut -d">" -f3 | cut -d"<" -f1

exit 0
