#!/bin/bash

cputemp_paths=($(ls -d /sys/class/thermal/thermal_zone*/))

#echo ${cputemp_paths[@]}

for path in ${cputemp_paths[@]}; do
  temp=$(cat ${path}/temp)
  temp=$((temp/1000))
  type=$(cat ${path}/type)
  printf "%s °C (type: %s) (%s)\n" "$temp" "$type" "$path"
done

exit 0
