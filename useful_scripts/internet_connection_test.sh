#!/bin/bash

# this script tests the stability of the internet connection
# with multiple attempts
# (this helps in cases when you have a connection for some seconds and then loose it)

LOOPS=5

for ((i=0; i<LOOPS; i++)); do
	if curl -sIL -m 1 https://google.com &>> /dev/null; then
		echo "Connection established."
	else
		echo "No internet connection."
	fi
	sleep 1
done

exit 0
