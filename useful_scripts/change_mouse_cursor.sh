#!/usr/bin/env bash

PS4='$LINENO: '

CONFIGFOLDER="${HOME}/.icons/default"
CONFIGFILE="index.theme"
CONFIGFILEPATH=$CONFIGFOLDER/$CONFIGFILE
CURRENT_THEME=$(grep "Inherits" ${CONFIGFILEPATH} | cut -d"=" -f2)

# make sure config folder exists
if test -d ${CONFIGFOLDER}; then
  echo "the config folder exists"
else
  echo "the config folder doesn't exist"
  mkdir -p ${CONFIGFOLDER}
fi

# make sure config file exists
if [ -f "${CONFIGFILEPATH}" ]; then
  echo "${CONFIGFILEPATH} exists."
fi

printf "current theme is %s\n" "$CURRENT_THEME"

echo "available themes:"
ls /usr/share/icons

read -p "choose a theme: " theme

printf "You chose the %s theme.\n" "$theme"


printf "[icon theme]\n" > "${CONFIGFILEPATH}"
printf "Inherits=%s" "$theme" >> "${CONFIGFILEPATH}"

echo "Changed mouse configuration."
echo "Please restart the x-server."
echo "(with this: sudo systemctl restart display-manager)"
