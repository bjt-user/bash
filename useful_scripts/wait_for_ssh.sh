#!/bin/bash

ssh_user="$1"
ssh_hostname="$2"

while ! ssh ${ssh_user}@${ssh_hostname}; do
        sleep 1
done

exit 0