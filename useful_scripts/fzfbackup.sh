#!/bin/bash

hostname=$(cat /etc/hostname)

remotehost=$(cat ~/.ssh/config | grep "Host " | fzf)
remotehost=${remotehost:5}

declare -a backupfolders
backupfolders+=(/home/bf/coding)
backupfolders+=(/home/bf/databases)
backupfolders+=(/home/bf/Documents)
backupfolders+=(/home/bf/electronics)
backupfolders+=(/home/bf/engineering)
backupfolders+=(/home/bf/.gnupg)
backupfolders+=(/home/bf/.password-store)
backupfolders+=(/home/bf/.task)
backupfolders+=(/home/bf/.taskrc)

BLACK=$(tput setaf 0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
LIME_YELLOW=$(tput setaf 190)
POWDER_BLUE=$(tput setaf 153)
BLUE=$(tput setaf 4)
MAGENTA=$(tput setaf 5)
CYAN=$(tput setaf 6)
WHITE=$(tput setaf 7)
BOLD=$(tput bold)
NORMAL=$(tput sgr0)
REVERSE=$(tput smso)
UNDERLINE=$(tput smul)

function confirmation() {
  read -p "Do you really want to continue? [y|n] "
  if [ "$REPLY" != "${REPLY#[Yy]}" ] ;then
    echo "Proceeding..."
  else
    echo "Aborting script..."
    exit 1
  fi
}

printf "The %ssource%s host is %s%s%s.\n" "${GREEN}" "$WHITE" "$GREEN" "$hostname" "${WHITE}"
printf "The %starget%s host is %s%s.\n" "$RED" "$WHITE" "$RED" "$remotehost" "$WHITE"
printf "%s%s%s %s-> %s%s%s%s\n" "$BOLD" "$GREEN" "$hostname" "$WHITE" "$RED" "$remotehost" "${WHITE}" "${NORMAL}"

confirmation

printf "The following folders will be backed up:\n"
printf "%s\n" "${backupfolders[@]}"

confirmation

printf "Performing dry-run...\n\n"

CMD="rsync -naAxvh --delete ${backupfolders[@]} bf@$remotehost:/home/bf"
echo $CMD
$CMD

confirmation

CMD="rsync -aAxvh --delete ${backupfolders[@]} bf@$remotehost:/home/bf"
echo $CMD
confirmation
if $CMD; then
  notify-send "Backup successfully completed."
else
  notify-send "Backup failed."
fi

exit 0
