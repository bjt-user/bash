#!/bin/bash

# script to scp directories or single files from multiple hosts

if [[ $1 == "-f" ]]; then
  mode=file
else
  mode=directory
fi

if [[ "$mode" == "directory" ]]; then
  echo "Which directory do you want to copy from multiple hosts?"
  read my_path
else
  echo "Which file do you want to copy from multiple hosts?"
  read my_path
fi

remotehosts=($(cat ~/.ssh/config | grep "Host " | cut -d" " -f2 | fzf -m --reverse | xargs))
path_basename=$(basename ${my_path})

if [[ "$mode" == "directory" ]]; then
  # scp an entire directory
  i=0
  for remotehost in "${remotehosts[@]}"
  do
    ((i++))
    printf "%s:\n" "$remotehost"
    scp -r "$remotehost":${my_path} "copied_${path_basename}_${remotehost}"
  done
else
  # scp a single file
  i=0
  for remotehost in "${remotehosts[@]}"
  do
    ((i++))
    printf "%s:\n" "$remotehost"
    scp "$remotehost":${my_path} "copied_${path_basename}_${remotehost}"
  done
fi

exit 0
