#!/bin/bash

function confirmation() {
  read -p "Do you really want to continue? [y|n] "
  if [ "$REPLY" != "${REPLY#[Yy]}" ] ;then
    echo "Proceeding..."
  else
    echo "Aborting script..."
    exit 1
  fi
}

exit 0
