If the source files are in the same dir a simple
```
ctags -R .
```
will work so you can jump from a function call to the definition.

It also works if you have a `lib` dir in the same directory where the sourced \
function is in.

#### search for tags files in higher dirs

Make sure you have this in your `vimrc` or language specific vim scripts:
```
set tags=tags;/
```

Then you dont have to be in the dir where the `tags` file is in.\
It will work even if the `tags` file is two levels higher up.
