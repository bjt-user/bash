Booleans don't seem to be properly supported in bash.\
You can use them, but they have a drawback as seen below.

```
#!/bin/bash

myflag=false

if !($myflag); then
  printf "your flag is NOT true!\n"
fi

if ($myflag); then
  printf "your flag is true!\n"
fi
```
**The problem is if the variable `$myflag` is empty, it will act as if it is true.**

So you should always remember to set a flag to `false` by default.
