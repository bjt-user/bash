## return code checking

#### shellcheck suggested rc checks

```
if ! cat myfile.txt; then
	echo 'error reading file, aborting'
	exit 1
fi
```

#### exit script if command was not successful

```
#!/bin/bash
 
cat myfile.txt || exit 1
   
echo 'script executed successfully'
   
exit 0
```
