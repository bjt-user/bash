#### working with multiple command line arguments

Going through every command line argument and acting accordingly:
```
while [ "$1" != "" ]; do
  case $1 in
    -h | --help)
      usage
      exit 0
      ;;

    -d | --debug)
      export PS4='+|${BASH_SOURCE##*/} ${LINENO} | ${FUNCNAME[0]}'
      set -x
      ;;
    *)
      printf "Unknown argument %s\n\n" "$1"
      usage
      exit 1
      ;;
  esac
  shift
done
```
The problem is that this way you can't act on the situation when the user does not give any arguments.

Another variant:
```
for i in "$@"; do
  case $i in
    -verbose|--verbose)
      verbose=true
    ;;
    -h|--help)
      usage
      exit 0
    ;;
  esac
done
```

#### example from "bocker"

https://github.com/p8952/bocker/blob/master/bocker

Functions are called `bocker_pull`, `bocker_init`, etc...

```
[[ -z "${1-}" ]] && bocker_help "$0"
case $1 in
	pull|init|rm|images|ps|run|exec|logs|commit) bocker_"$1" "${@:2}" ;;
	*) bocker_help "$0" ;;
esac
```

#### simple example with one expected argument (with usage and main)

```
#!/bin/bash

function parse_args() {
        [[ -z "${1-}" ]] && usage "$0"

        case $1 in
        dev | test | prod) main "$1" ;;
        *) usage "$0" ;;
        esac
}

function usage() {
        printf "Usage:\n%s [dev/test/prod]\n" "$1"
        exit 1
}

function main() {
        stage="$1"
        echo "stage: ${stage}"
}

parse_args "$@"

exit 0
```

It expects the first cli argument to be either "dev", "test" or "prod".\
If additional arguments are given, they will be ignored.

#### getopts

"a:b:cf:": Specifies valid options:

`a`, `b`, and `f` expect arguments (: after the letter).\
`c` does not expect an argument.

`$OPTARG`: Contains the argument for the option when applicable.\
`\?`: Handles invalid options.\
`:`: Handles missing arguments.

example:
```
while getopts "a:b:cf:" opt; do
        case $opt in
        a) echo "Option -a was passed with argument: $OPTARG" ;;
        b) echo "Option -b was passed with argument: $OPTARG" ;;
        c) echo "Option -c was passed (no argument expected) (OPTARG: $OPTARG)" ;;
        f) echo "Option -f was passed with argument: $OPTARG" ;;
        \?)
                echo "Invalid option: -$OPTARG" >&2
                exit 1
                ;;
        :)
                echo "Option -$OPTARG requires an argument." >&2
                exit 1
                ;;
        esac
done
```

#### example with function, multiple cli args and getopts

You can declare variables in your parse_args function \
then assign them within the getopts while loop \
and then pass them to the main function:
```
function parse_args() {
        local check_mode
        local stage

        check_mode=false

        [[ -z "${1-}" ]] && usage "$0"

        while getopts "cs:" opt; do
                case $opt in
                c)
                        check_mode=true
                        ;;
                s)
                        stage="$OPTARG"
                        ;;
                \?)
                        echo "Invalid option: -$OPTARG" >&2
                        exit 1
                        ;;
                :)
                        echo "Option -$OPTARG requires an argument." >&2
                        exit 1
                        ;;
                esac
        done

        case $stage in
        dev | test | prod) main "$stage" "$check_mode" ;;
        *) usage "$0" ;;
        esac
}
```

This function will then be the only function that is going to be called \
in the entire script:
```
parse_args "$@"
```
