#### check if directory exists

```
#!/usr/bin/env bash

if test -d test; then
  echo "the test folder exists"
else
  echo "the test folder doesn't exist"
fi
```
This will check if a folder named `test` is in the current directory.

#### save directories in an array

```
cputemp_paths=($(ls -d /sys/class/thermal/thermal_zone*/))

echo ${cputemp_paths[@]}
```
