**Always use `read -r` to not escape backslashes**

Get input from the user and save it in a variable:
```
#!/bin/bash

printf "What is your name?\n"
read myanswer
printf "%s\n" "$myanswer"
```

get the key that the user has pressed without the need to press enter after that:\
(does not work for arrowkeys)
```
#!/bin/bash

read -n 1 mykey

printf "\nYou pressed %s\n" "$mykey"
```

#### multiline user input

```
#!/bin/bash

declare -a inputs

while read line
do
  inputs+=($line)
  if [[ $line == "" ]]; then
    break
  fi
done < "${1:-/dev/stdin}"

printf "%s\n" "${inputs[@]}"
```
This reads all lines you enter until you just type enter without entering characters.\
Each line is an element of the array `inputs`.

#### adding a short prompt

adding a short prompt into the read statement with `read -p`:
```
read -p "Are you sure you want to do this? " affirmation
if [[ "$affirmation" == "y" ]]; then
  echo "ok, lets proceed."
else
  echo "STOP"
fi
```

#### check answers

This will check if the answer starts with a `y` or `Y` and then conclude that you mean "yes".
```
read -p "do you want to proceed? " answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
  echo Yes
else
  echo No
fi
```

`${answer#[Yy]}` removes everything from the string until a `y` or `Y` appears.\
https://wiki.bash-hackers.org/syntax/pe#substring_removal \
So the string will not be the same when it has `y` or `Y` in it.

Maybe I can come up with a more readable version of this check.

Standard function you can use:
```
function confirmation() {
  read -p "Do you really want to continue? [y|n] "
  if [ "$REPLY" != "${REPLY#[Yy]}" ] ;then
    echo "Proceeding..."
  else
    echo "Aborting script..."
    exit 1
  fi
}
```

#### using $REPLY

If you dont use a variable with `read` it will be saved in a variable called `$REPLY`.
```
$ read -p "foo? "
foo? bar
$ echo $REPLY
bar
```
