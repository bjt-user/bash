This worked and the date is updated:
```
#!/bin/bash

# arg1: log level (INFO/WARN/ERROR)
# arg2: log message
function log_message() {
    script_name=$(basename "$0")
    echo -e "$(date +%Y-%m-%d" "%T.%3N) - ${script_name} - $1 - $2"
}

TEST_VAR="foo bar"

log_message "INFO" "hello im under the water"
sleep 5
log_message "WARN" "please help me"
sleep 3
log_message "INFO" "TEST_VAR: ${TEST_VAR}"

exit 0
```

(`echo -e` so that you can potentially use `/n` in your message to print multiple lines)


The date does not update when a variable is used...
```
#!/bin/bash

script_name=$(basename "$0")
log_prefix="${script_name} $(date +%Y-%m-%d" "%T.%3N): "
echo "${log_prefix} connect succeeded"
sleep 5
echo "${log_prefix} connect succeeded"
sleep 5
echo "${log_prefix} connect succeeded"

exit 0
```

To make stdout messages more usable for a central log management:
```
#!/bin/bash

script_name=$(basename "$0")
log_prefix="$(date +%Y-%m-%d" "%T.%3N) - ${script_name} - "
echo "${log_prefix} INFO - connect succeeded"

exit 0
```
Log level is not saved in a variable in this example, it is written before each log message.

https://stackoverflow.com/questions/1765689/what-is-the-best-practice-for-formatting-logs

```
2005-03-19 15:10:26,618 - simple_example - DEBUG - debug message
2005-03-19 15:10:26,620 - simple_example - INFO - info message
2005-03-19 15:10:26,695 - simple_example - WARNING - warn message
2005-03-19 15:10:26,697 - simple_example - ERROR - error message
2005-03-19 15:10:26,773 - simple_example - CRITICAL - critical message
```

#### ISO 8601

https://de.wikipedia.org/wiki/ISO_8601

this only shows 0s as decimal places:
```
date +"%Y-%m-%dT%H:%M:%S%z"
```

better do it like this:
```
date +"%Y-%m-%dT%T+%4N"
```

to use it in scripts:
```
script_name=$(basename "$0")
log_prefix="$(date +"%Y-%m-%dT%T+%3N") - ${script_name} - "
```
