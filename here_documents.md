"here documents" might be a convenient way to execute commands as another user in a script
```
#!/bin/bash

sudo -u tester sh << EOF
whoami
echo "I am ${UID}"
EOF

exit 0
```
But in this example the UID is not from the tester user, because the environment has not been sourced correctly.

This also works for sqlplus:
```
sqlplus myuser << EOF
select * from my_table where ident like '%mycolumn%';
select count(*) from my_table where ident like '%mycolumn%';
EOF
```

#### indenting with here documents

You need a dash (`-`) after the two arrows (`<<`):
```
function download_file() {
        sftp -v -b - my-sftp-server <<-EOF
                get /tmp/foo.txt
        EOF

        if [[ $? -ne 0 ]]; then
                echo "Failed to download file"
                exit 1
        fi
}
```
This way you can indent your scripts normally.

#### TODO: checking return code of here documents

(conflicts with `shfmt` and `vim` syntax highlighting)

Shellcheck friendly checking of return codes,\
without using `$?`:
```
function download_file() {
        if sftp -v -b - my-sftp-server <<-EOF ; then
                get /tmp/foo.txt
        EOF
                echo "DOWNLOAD SUCCESS"
                return 0
        else
                echo "DOWNLOAD FAILED"
                return 1
        fi
}
```

The `EOF` can also be indented:
```
if sftp -v -b - my-sftp-server <<-EOF ; then
    get /tmp/foo.txt
    EOF

    echo "DOWNLOAD SUCCESS"
    return 0
fi
```

Problem:
`shfmt` will format like this
```
if sftp -v -b - my-sftp-server <<-EOF; then
```
and then the `vim` syntax highlighting will be destroyed...
