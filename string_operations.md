https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html

#### How to get the length of a string

```
my_string="arch"

printf "%s" "${#my_string}"
```

#### Substrings

```
newstring=${my_string:0:2}
```

remove the last nine characters of a string:
```
answer=${answer::-9}
```

#### multi-line string

```
#!/bin/bash

longstring="this is a very very very long \
maybe even unnecessary long string \
but sometimes you have to test this stuff"

printf "the long string: %s" "$longstring"
```

#### parameter expansion

https://wiki.bash-hackers.org/syntax/pe


for example:\
this removes everything until the last `/`:
```
mystring="/home/bofo/scripts/testfolder"

strippedstring="${mystring##*/}"

printf "%s\n" "$strippedstring"
```

If you use only one `#` it should remove only everything until the first `/`.\
According to the link you can also do it backwards and much more...

#### remove everything from the end of a string until...

This removes the file name of a path:
```
${company_path%/*}
```

It looks like you can add more % signs to remove until the second slash, the third slash and so on...

#### remove extension of a file name
```
${myfilename%.*}
```

#### remove everything but the first word of the string

```
myvar="hello dark world"
```

```
$ echo ${myvar%% *}
hello
```

#### replace a substring inside a string

This will remove all `\n`s inside a string (replace with nothing ("")):
```
mystring=${mystring//\\n/}
```
(the `\n` is a escaped with an additional `\`)\
the additional `/` in the `//` after the variable name means that it will replace all occurences and not only the first)

#### get last X characters of a string

```
firststring="low-level"
secondstring="high-level"

echo ${firststring: -5}
echo ${secondstring: -5}
```

#### make string upper case

```
name="lower case"
echo "${name^^}"
```

#### string arrays

For string arrays you can do a lot with parameter expansion.\
For example add or remove a substring to each element.

This will add the prefix "prefix_" to each array element:
```
array=( "${array[@]/#/prefix_}" )
```

#### split string

This is shellcheck compatible:
```
#!/bin/bash

text="apple,orange,banana"

# Replace all commas with a space
split_text="${text//,/ }"

# Read the split text into an array safely
read -ra fruits <<<"$split_text"

# Loop through the array elements
for fruit in "${fruits[@]}"; do
	echo "$fruit"
done

exit 0
```

#### concatenate strings

Concatenate a string to a variable.\
For example for an email text that goes over several lines:
```
failed_hosts=3

mail_text+="hello,\n"
mail_text+="we have ${failed_hosts} failures.\n"
mail_text+="more text\n"
mail_text+="goodbye"

echo -e "$mail_text"
```
