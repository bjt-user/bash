```
case $1 in
  delete)
    delete_section $2
    ;;
  read)
    read_section $2
    ;;
esac
```

#### default value (if no case hits)

```
#!/usr/bin/bash

expression="$1"

case $expression in
  foo)
    echo "its foo"
    ;;

  bar)
    echo "we have bar"
    ;;
  *)
    echo "we have nothing"
    ;;
esac

exit 0
```