#### stdout and stderr
Programs can print messages to `stdout` or `stderr`.\
Usually `stderr` is for errors and `stdout` for normal messages.\
But for example the program `stow` only outputs to `stderr` for all kind of messages.\

***

Putting this `2>/dev/null` at the end of your command will not print error messages of that command to the terminal.\
*Note that you will have to this before you pipe the command into another command.*
*(for example: ls -d */ 2>/dev/null | wc -l)*

File descriptor 1 is the standard output (stdout).\
File descriptor 2 is the standard error (stderr).

`/dev/null` is a so-called "null device".\
The null device is a device file that **discards all data written to it** but reports that the write operation succeeded.\
https://en.wikipedia.org/wiki/Null_device

#### redirect STDOUT and STDERR to logfile

With newer bash version (version 3 or 4) you can do
```
examplecommand &>> "${logfile}"
```
that should redirect `stdout` AND `stderr` to the logfile.

#### piping stderr

Normally you can't pipe `stderr`.\
`command | grep "error"` will not work
but
```
command |& grep "error"
```
will work and grep also the `stderr`

***

#### redirecting to a logfile AND stdout

To redirect to a logfile but still view the output of the command or script on stdout.

```
$ echo "double output" | tee output.file
double output
$ cat output.file 
double output
```

If you want to include `stderr`:
```
program [arguments...] 2>&1 | tee outfile
```
including `stderr` but easier to remember:
```
/path/to/script.sh |& tee /path/to/log/script_$(date +%Y%m%d).log
```

=> but this will not put the process in the background and is probably not recommended as the process will stop when you do <kbd>ctrl</kbd> + <kbd>c</kbd> or when you lose your ssh session
***
#### exclude stderr

```
java -cp lib/catalina.jar org.apache.catalina.util.ServerInfo 2>/dev/null
```

#### nohup

Send the command to the background and do not let it come to the foreground with disrupting outputs to stderr.
```
nohup my command &>> mylog_$(date +%Y%m%d_%H%M%S).log &
```
