You can use different styles of for loops in bash.


#### C-style for-loop

```
for ((i=0; i<$n; i++)); do
  echo $i
done
```

#### loops with in keyword

```
for i in {1..10}; do
    echo "${i}"
done
```

#### iterate through an array

```
i=0
for element in "${myarray[@]}"
do
  ((i++))
  printf "loop round %s: " "$i"
  printf "%s\n" "$element"
done
```

#### loop over files

```
#!/bin/bash

files="/home/myuser/*"

# to also loop over hidden files
shopt -s dotglob

for file in $files
do
  echo "${file}"
done
```
