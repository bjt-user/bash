https://unix.stackexchange.com/questions/24684/confusing-use-of-and-operators

The right side of `&&` will only be evaluated if the exit status of the left side is zero (i.e. true).\
`||` is the opposite: it will evaluate the right side only if the left side exit status is non-zero (i.e. false).

```
$ cat /tmp/noexist || echo "the file doesnt exist"
cat: /tmp/noexist: No such file or directory
the file doesnt exist
```

```
$ cat /tmp/noexist && echo "the file exists"
cat: /tmp/noexist: No such file or directory
```

This is very handy, because you can do things like
```
gcc -Wall main.c && ./a.out
```
And the compiled program will only be executed if the compilation completed with an exit code of zero.
