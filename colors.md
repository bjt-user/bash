use the `tput` command
```
man tput
```

You could put this in your script:
```
BLACK=$(tput setaf 0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
LIME_YELLOW=$(tput setaf 190)
POWDER_BLUE=$(tput setaf 153)
BLUE=$(tput setaf 4)
MAGENTA=$(tput setaf 5)
CYAN=$(tput setaf 6)
WHITE=$(tput setaf 7)
BRIGHT=$(tput bold)
NORMAL=$(tput sgr0)
BLINK=$(tput blink)
REVERSE=$(tput smso)
UNDERLINE=$(tput smul)
```

And then just do
```
printf "%sHello%sagain\n" "$RED" "$YELLOW"
```

**WARNING: ALWAYS END YOUR SCRIPTS WITH A WHITE AND NORMAL COLOR BECAUSE THE COLOR CHANGE IS PERMANENT!**
