#### how to execute the content of a variable?

```
#!/bin/bash

MYVAR="echo test"

$MYVAR
```

But this will not work correctly when there are double or single quotes in the string.

This will
```
#!/bin/bash

MYVAR="echo \"test\""

${MYVAR}

exit 0
```
will output:
```
"test"
```

#### use eval

It works with `eval` though:
```
#!/bin/bash

MYVAR="echo \"test\""

eval "${MYVAR}"

exit 0
```

but `eval` is supposed to be dangerous and buggy.

#### put it in an array

The correct solution according to stackoverflow is to put it into an array and expand that array.

https://stackoverflow.com/a/44055875/21120612
