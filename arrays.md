#### shellcheck compatible array creation

https://www.shellcheck.net/wiki/SC2207

`shellcheck` wants you to build arrays like this.\
This works for putting every line in a file into an array:
```
#!/bin/bash

mapfile -t files < <(cat files_to_move.txt)

for file in "${files[@]}"; do
        echo "${file}"
        mv -v /path/to/dir/${file} files/.
done

exit 0
```
When the elements are separated by a different separator than `/n`, you might have to do it differently.

This works for files in a directory:
```
mapfile -t files < <(find . -name "*.sh")

for file in "${files[@]}"; do
        echo "${file}"
done
```

#### declare an array

```
names[0]="hans"
names[1]="wurst"
names[2]="peter"

printf "second name is %s\n" "${names[1]}"
printf "third name is %s\n" "${names[2]}"
printf "first name is %s\n" "${names[0]}"
```

#### compound arrays
A way to declare arrays without typing numbers and just adding a new array element.
```
declare -a names
names+=(hans)
names+=(marko)
names+=(julian)

printf "the second name is %s\n" "${names[1]}"
```

For assigning string variables you should add double quotes:
```
names+=("$myname")
```
Otherwise spaces will act as separators between array elements and if the string contains spaces you will have multiple array elements from one variable when you just wanted the variable to be one element.

You need double quotes in your for loop, because this will NOT work:
```
#!/bin/bash

declare -a string_array

string_array+=("ps -ef")
string_array+=("systemctl status sshd")

for string in ${string_array[@]}; do
        echo "${string}"
done

exit 0
```

The for loop needs to look like this:
```
for string in "${string_array[@]}"; do
        echo "${string}"
done
```

#### iterate through an array
[see for_loops.md](for_loops.md#iterate-through-an-array)

#### Put all files in the current directory into an array
```
files=(*)
printf '%s\n' "${files[@]}"
```

put only files with .sh ending into the array:
```
shfiles=(*.sh)
printf '%s\n' "${shfiles[@]}"
```

#### put all files in a specific directory into an array
```
myarray=($(ls ${HOME}/scripts/))

printf "%s\n" "${myarray[1]}"
```
The `()` make it into an array.

This works for **hidden files (dotfiles)** (excluding the `.` and `..` that are shown with `-a` but not `-A`):
```
myarray=($(ls -A ${HOME}/scripts/))

printf "%s\n" "${myarray[1]}"
```

You can also do it like this, if want to **split a string** with spaces or new lines into an array:
```
mystring=$(ls -A ${HOME}/scripts/)

myarray=($mystring)

printf "%s\n" "${myarray[@]}"
```

But it is probably better to use the `find` command to put files in an array.

#### substring removal on a string array

This will remove a part of each element of the array:
```
#!/bin/bash

myarray=(file_one.txt file_two.txt file_three.txt)

echo ${myarray[@]}

myarray=(${myarray[@]##*.})

echo ${myarray[@]}

exit 0

```

Watch the `()` closely at the second array assignment.\
The `()` at the assignment of each array are absolutely crucial otherwise you will encounter very weird behavior!

#### make string into array

I had a string like this:
```
#!/bin/bash

cities="Berlin Paris"
cities_array=(${cities})

echo ${cities_array[1]}

exit 0
```
And the `()` help to make it into an array.

Seems to only work when there are spaces between the elements.

***

#### split string by new lines and save to array

This was text file with commands in it that were separated by new line characters.\
By default IFS is a space.
```
#!/bin/bash

SAVE_IFS=${IFS}
IFS=$'\n'
commands=($(cat textfile.txt))
IFS=${SAVE_IFS}

i=0
for command in "${commands[@]}"; do
        echo "loop ${i}"
        echo "${command}"
        eval ${command}
        ((i++))
        echo
done

exit 0
```

#### check number of elements in the array

```
files=($(find . -name "*csv" -exec basename {} \;))

echo "there are ${#files[@]} elements in the array."
```
