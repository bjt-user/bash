#### do not overwrite existing files

This will not overwrite a file with the name `mycopy.txt`:
```
cp -n myfile.txt mycopy.txt
```
(it will not check the content of the existing file)
