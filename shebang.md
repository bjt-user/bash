Having the `-l` flag in the shebang will create a login shell when executing the script
```
#!/bin/bash -l
```
this helps to preserve environment variables and might be useful in other ways sometimes

The `-l` has been proven useful in a lot of circumstances.\
It might make sense to use it as a standard. (at least in a business environment)

#### POSIX-shell /bin/sh

`/usr/bin/sh` ist nur ein Symlink auf `/usr/bin/bash`
```
ls -la /usr/bin | grep 'bash'
-rwxr-xr-x 1 root root    1112880 Nov 22 16:26 bash
-r-xr-xr-x 1 root root       7246 Nov 22 16:26 bashbug
lrwxrwxrwx 1 root root          4 Nov 22 16:26 rbash -> bash
lrwxrwxrwx 1 root root          4 Nov 22 16:26 sh -> bash
```

Dennoch sind beim shebang `/usr/bin/sh` Bash-Features disabled. (z.B. Arrays)
